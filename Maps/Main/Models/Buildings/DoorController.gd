extends Spatial

var animation_player
var ending_stop

func _ready():
	animation_player = $AnimationPlayer
	ending_stop = $EndingStop/CollisionShape
	animation_player.play_backwards("DoorOpen")
	GameEvents.connect("area_finished", self, "open_door")
	GameEvents.connect("last_item_collected", self, "open_ending")

func open_ending():
	ending_stop.disabled = true

func open_door():
	animation_player.play("DoorOpen")
