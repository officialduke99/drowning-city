![Alt text](https://img.itch.zone/aW1nLzc1NTA3MzMucG5n/original/91NEsj.png)

This is the project repository for the game [Drowning City](https://officialduke99.itch.io/drowning-city).  
The game is created and built using the [Godot](https://godotengine.org/) game engine.  

All of the code is licensed under the Unlicense.  
All of the assets are licensed under the Creative Commons Zero v1.0 Universal license.  
  
  
## Contents

Here you can find the entire [Godot Project](https://gitlab.com/officialduke99/drowning-city/-/blob/main/project.godot), an early [design document](https://gitlab.com/officialduke99/drowning-city/-/blob/main/DrowningCityDocs.drawio), all the [images used for promoting the game](https://gitlab.com/officialduke99/drowning-city/-/tree/main/PromoImages) and all of the game's [3D](https://gitlab.com/officialduke99/drowning-city/-/tree/main/Maps/Main) and [other](https://gitlab.com/officialduke99/drowning-city/-/tree/main/Assets) assets.