extends Area

var items = []
export(int) var area_number = 0
var items_collected : bool = false

func _enter_tree():
	GameEvents.areas.append(area_number)

func _ready():
	GameEvents.connect("area_finished", self, "update_item_amount")
	update_item_amount()

func update_item_amount():
	if GameEvents.active_area == area_number:
		items = self.get_children()
		items.remove(0)
		GameEvents.item_amount = items.size()
		items_collected = true
		GameEvents.emit_signal("ui_fade", 0, "ItemAmount", 1)
		GameEvents.emit_signal("ui_set_text", str(GameEvents.item_collected) + "/" + str(GameEvents.item_amount + GameEvents.item_collected), "ItemAmount")
		

func _on_AreaController_body_entered(body):
	if items_collected:
		GameEvents.emit_signal("area_exited")
		if not GameEvents.areas.find(GameEvents.active_area) < 0:
			GameEvents.emit_signal("ui_fade", 0, "ItemAmount", 0)
