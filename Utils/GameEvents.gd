extends Node

var item_collected : int = 0
var item_amount : int = 0
var areas : Array = []
var active_area : int
var player_moving : bool = 0
var player_position : Vector3
var is_interacting : bool = false

signal interacted(area)
signal ui_fade(duration, element, in_out)
signal ui_set_text(text, element)
signal area_finished()
signal area_exited()
signal game_ended()
signal last_item_collected()
signal fall_hole()

func _ready():
	active_area = 1
	connect("area_finished", self, "on_area_finished")
	connect("area_exited", self, "on_area_exited")
#
func on_area_finished():
	if active_area == 2:
		GameEvents.emit_signal("last_item_collected")
	active_area += 1
	item_collected = 0
	emit_signal("ui_fade", 0, "ItemAmount", 1)
	emit_signal("ui_set_text", str(item_collected) + "/" + str(item_amount + item_collected), "ItemAmount")
	

func on_area_exited():
	if areas.find(active_area) < 0:
		on_game_finished()

func on_game_finished():
	yield(get_tree().create_timer(1.0), "timeout")
	is_interacting = true
	emit_signal("ui_fade", 4, "BlackRect", 0)
	yield(get_tree().create_timer(2.0), "timeout")
	emit_signal("game_ended")
