extends Area

class_name Key

export(String) var text = null
var distance_to_player: float
onready var light = $OmniLight
onready var audio_player = $AudioStreamPlayer

func _ready():
	GameEvents.connect("interacted", self, "_on_interacted")
	distance_to_player = self.transform.origin.distance_to(GameEvents.player_position)

func _process(_delta):
	if GameEvents.player_moving:
		distance_to_player = self.global_transform.origin.distance_to(GameEvents.player_position)
		distance_to_player = clamp(distance_to_player, 0, 10)
		light.light_energy = 1 / distance_to_player - 0.1
	


func _on_interacted(area):
	if area.name == self.name:
		GameEvents.item_amount -= 1
		GameEvents.item_collected += 1
		self.hide()
		self.collision_layer = 2
		audio_player.play()
		GameEvents.emit_signal("ui_set_text", str(GameEvents.item_collected) + "/" + str(GameEvents.item_amount + GameEvents.item_collected), "ItemAmount")
		if GameEvents.item_amount < 1:
			GameEvents.emit_signal("area_finished")
		GameEvents.emit_signal("ui_set_text", text, "Text")
		GameEvents.emit_signal("ui_fade", 1, "Text", 0)
		yield(get_tree().create_timer(1.0), "timeout")
		GameEvents.emit_signal("ui_fade", 2, "Text", 1)
		queue_free()
		
