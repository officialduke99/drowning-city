extends RayCast

func _input(event):
	var collided_area : Area = get_collider()
	if collided_area and Input.is_action_pressed("pick_up") and not GameEvents.is_interacting:
		GameEvents.emit_signal("interacted", collided_area)
