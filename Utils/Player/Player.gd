extends KinematicBody

# This player script is borrowed from 
# https://github.com/Whimfoome/godot-FirstPersonStarter
# and modified for my purposes
# Thank you Whimfoome <3 

###################-VARIABLES-####################

enum {MOVE, PAUSE}
var state = MOVE

# Camera
export(float) var mouse_sensitivity = 8.0
export(NodePath) var head_path = "Head"
export(NodePath) var cam_path = "Head/Camera"
export(float) var FOV = 80.0
var mouse_axis := Vector2()
onready var head: Spatial = get_node(head_path)
onready var cam: Camera = get_node(cam_path)
var joy_starting_pos = Vector2.ZERO
var joy_speed_multiplier = 30
var joy_dead_zone = 0.2

# Move
var velocity := Vector3()
var direction := Vector3()
var move_axis := Vector2()
var snap := Vector3()
var default_position := Vector3()
var delayed_position := Vector3()
var current_position := Vector3()
# Walk
const FLOOR_MAX_ANGLE: float = deg2rad(40)
export(float) var gravity = 8.0
export(int) var walk_speed = 7
export(int) var acceleration = 1
export(int) var deacceleration = 3
export(float, 0.0, 1.0, 0.05) var air_control = 0.1
export(int) var jump_height = 4
var _speed: int
var _is_jumping_input := false

##################################################

# Called when the node enters the scene tree
func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	cam.fov = FOV
	_speed = walk_speed
	default_position = Vector3(self.global_transform.origin.x, self.global_transform.origin.y, self.global_transform.origin.z)
	GameEvents.connect("fall_hole", self, "on_hole_fall")
	GameEvents.emit_signal("ui_fade", 2, "StartLogo", 1)
	GameEvents.emit_signal("ui_fade", 0, "BlackRect", 0)
	GameEvents.emit_signal("ui_set_text", "\"If the ocean dies, we all die.\"\n - Captain Paul Watson, Sea Shepherd Conservation Society", "Text")
	yield(get_tree().create_timer(2.0), "timeout")
	GameEvents.emit_signal("ui_fade", 2, "Text", 0)
	yield(get_tree().create_timer(4.0), "timeout")
	GameEvents.emit_signal("ui_fade", 2, "Text", 1)
	yield(get_tree().create_timer(1.0), "timeout")
	GameEvents.emit_signal("ui_fade", 2, "BlackRect", 1)
	yield(get_tree().create_timer(2.0), "timeout")
	GameEvents.emit_signal("ui_set_text", str(GameEvents.item_collected) + "/" + str(GameEvents.item_amount), "ItemAmount")
	GameEvents.emit_signal("ui_fade", 2, "ItemAmount", 0)
	


# Called every frame. 'delta' is the elapsed time since the previous frame
func _process(_delta: float) -> void:
	move_axis.x = Input.get_action_strength("move_forward") - Input.get_action_strength("move_backward")
	move_axis.y = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	
	if Input.is_action_just_pressed("move_jump"):
		_is_jumping_input = true


# Called every physics tick. 'delta' is constant
func _physics_process(delta: float) -> void:
	walk(delta)
	if Input.get_connected_joypads().size() > 0:
		var xAxis = Input.get_joy_axis(0, JOY_AXIS_2)
		if abs(xAxis) > joy_dead_zone:
			if xAxis < 0:
				mouse_axis.x -= abs(xAxis) * joy_speed_multiplier
			if xAxis > 0:
				mouse_axis.x += abs(xAxis) * joy_speed_multiplier
		var yAxis = Input.get_joy_axis(0, JOY_AXIS_3)
		if abs(yAxis) > joy_dead_zone:
			if yAxis < 0:
				mouse_axis.y -= abs(yAxis) * joy_speed_multiplier
			if yAxis > 0:
				mouse_axis.y += abs(yAxis) * joy_speed_multiplier
		camera_rotation()


# Called when there is an input event
func _input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		mouse_axis = event.relative
		camera_rotation()


func walk(delta: float) -> void:
	
	check_position()
	direction_input()
	
	if is_on_floor():
		snap = -get_floor_normal() - get_floor_velocity() * delta
		
		# Workaround for sliding down after jump on slope
		if velocity.y < 0:
			velocity.y = 0
		
		jump()
	else:
		# Workaround for 'vertical bump' when going off platform
		if snap != Vector3.ZERO && velocity.y != 0:
			velocity.y = 0
		
		snap = Vector3.ZERO
		
		velocity.y -= gravity * delta
	
	accelerate(delta)
	
	match state:
		MOVE:
			velocity = move_and_slide_with_snap(velocity, snap, Vector3.UP, true, 4, FLOOR_MAX_ANGLE)
		PAUSE:
			pass
	
	_is_jumping_input = false
	current_position = self.global_transform.origin
	yield(get_tree().create_timer(0.1), "timeout")
	delayed_position = current_position
	if GameEvents.is_interacting:
		state = PAUSE
	elif not GameEvents.is_interacting:
		state = MOVE


func camera_rotation() -> void:
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return
	if mouse_axis.length() > 0:
		var horizontal: float = -mouse_axis.x * (mouse_sensitivity / 100)
		var vertical: float = -mouse_axis.y * (mouse_sensitivity / 100)
		
		mouse_axis = Vector2()
		
		rotate_y(deg2rad(horizontal))
		head.rotate_x(deg2rad(vertical))
		
		# Clamp mouse rotation
		var temp_rot: Vector3 = head.rotation_degrees
		temp_rot.x = clamp(temp_rot.x, -90, 90)
		head.rotation_degrees = temp_rot


func direction_input() -> void:
	direction = Vector3()
	var aim: Basis = get_global_transform().basis
	if move_axis.x >= 0.5:
		direction -= aim.z
	if move_axis.x <= -0.5:
		direction += aim.z
	if move_axis.y <= -0.5:
		direction -= aim.x
	if move_axis.y >= 0.5:
		direction += aim.x
	direction.y = 0
	direction = direction.normalized()


func accelerate(delta: float) -> void:
	# Where would the player go
	var _temp_vel: Vector3 = velocity
	var _temp_accel: float
	var _target: Vector3 = direction * _speed
	
	_temp_vel.y = 0
	if direction.dot(_temp_vel) > 0:
		_temp_accel = acceleration
		
	else:
		_temp_accel = deacceleration
	
	if not is_on_floor():
		_temp_accel *= air_control
	
	# Interpolation
	_temp_vel = _temp_vel.linear_interpolate(_target, _temp_accel * delta)
	
	velocity.x = _temp_vel.x
	velocity.z = _temp_vel.z
	
	# Make too low values zero
	if direction.dot(velocity) == 0:
		var _vel_clamp := 0.01
		if abs(velocity.x) < _vel_clamp:
			velocity.x = 0
		if abs(velocity.z) < _vel_clamp:
			velocity.z = 0


func jump() -> void:
	if _is_jumping_input:
		velocity.y = jump_height
		snap = Vector3.ZERO

func check_position():
	delayed_position = delayed_position.snapped(Vector3(0.1, 0.1, 0.1))
	current_position = current_position.snapped(Vector3(0.1, 0.1, 0.1))
	if delayed_position != current_position:
		GameEvents.player_moving = 1
		GameEvents.player_position = current_position
	else:
		GameEvents.player_moving = 0

func on_hole_fall():
	self.global_transform.origin = default_position

