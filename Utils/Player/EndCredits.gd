extends CanvasLayer

onready var tween = $Tween

func _ready():
	GameEvents.connect("game_ended", self, "scroll")
	
func scroll():
	print("game_ended")
	tween.interpolate_property(self, "offset", Vector2(0, 1000), Vector2(0, -4020), 50, Tween.TRANS_LINEAR)
	tween.start()


func _on_Tween_tween_all_completed():
	yield(get_tree().create_timer(2.0), "timeout")
	get_tree().quit()
