extends CanvasLayer

export(NodePath) var element_path
export(NodePath) var tween_path

onready var tween = get_node(tween_path)
onready var local_element : CanvasItem = get_node(element_path)

func _ready():
	GameEvents.connect("ui_fade", self, "fade_choose")
	GameEvents.connect("ui_set_text", self, "set_text")
	local_element.modulate = Color(1.0, 1.0, 1.0, 0.0)

func fade_choose(duration, element, in_out):
	if in_out == 0:
		fade_in(duration, element)
	else:
		fade_out(duration, element)

func fade_out(duration, element):
	if element == self.name:
		tween.interpolate_property(local_element, "modulate", Color(1.0, 1.0, 1.0, 1.0), 
		Color(1.0, 1.0, 1.0, 0.0), duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()

func fade_in(duration, element):
	if element == self.name:
		tween.interpolate_property(local_element, "modulate", Color(1.0, 1.0, 1.0, 0.0), 
		Color(1.0, 1.0, 1.0, 1.0), duration, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()

func set_text(text, element):
	if local_element.is_class("Label") and self.name == element:
		local_element.text = text
		
